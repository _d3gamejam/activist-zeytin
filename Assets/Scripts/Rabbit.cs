using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rabbit : MonoBehaviour
{
    enum AnimationState
    {
        Sit,
        Talk
    }
    private AnimationState _animationState;
    private AnimationState animationState
    {
        get => _animationState;
        set
        {
            var isSameAnim = false;
            if (_animationState == value)
                isSameAnim = true;
            _animationState = value;
            if (animator && !isSameAnim)
                animator.CrossFade(value.ToString(), .1f, 0, 0);
        }
    }
    [SerializeField] private Animator animator;

    internal void SetSit()
    {
        animationState = AnimationState.Sit;
    }
    internal void SetTalk()
    {
        animationState = AnimationState.Talk;
    }
}
