using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Character : MonoBehaviour
{
    enum AnimationState
    {
        Idle,
        Sit,
        Die,
        Walk,
        Ghost
    }
    enum Place
    {
        Port,
        Boat,
        Ghost
    }
    private AnimationState _animationState;
    private AnimationState animationState
    {
        get => _animationState;
        set
        {
            var isSameAnim = false;
            if (_animationState == value)
                isSameAnim = true;
            _animationState = value;
            if (animator && !isSameAnim)
                animator.CrossFade(value.ToString(), .1f, 0, 0);
        }
    }
    private Place _currentPlace;
    private Place currentPlace
    {
        get => _currentPlace;
        set
        {
            _currentPlace = value;

            switch (_currentPlace)
            {
                case Place.Port:
                    {
                        characterSpeed = 1f;
                        CameraManager.Instance.SetCamera(VirtualCameras.PortCamera);
                        UIManager.Instance.SetDistanceBarStatus(false);
                        GameLogic.Instance.SetUsedDirtCountZero();
                    }
                    break;
                case Place.Boat:
                    {
                        characterSpeed = initialSpeed;
                        CameraManager.Instance.SetCamera(VirtualCameras.FollowCamera);
                        UIManager.Instance.SetDistanceBarStatus(true);
                    }
                    break;
                case Place.Ghost:
                    {
                        characterSpeed = initialSpeed;
                        CameraManager.Instance.SetCamera(VirtualCameras.FollowCamera);
                        UIManager.Instance.SetDistanceBarStatus(false);

                        var newPos = body.position;
                        newPos.y += 1f;

                        animator.transform.DOMoveY(body.position.y + .5f, 1f).OnComplete(() => MoveActive = true);
                    }
                    break;
            }
        }
    }
    private int DirtCoin
    {
        get => PlayerPrefs.GetInt("DirtCoin", 0);
        set
        {
            PlayerPrefs.SetInt("DirtCoin", value);
            onCurrencyUpdated?.Invoke(value);
        }
    }

    public bool IsAlive { get; private set; }
    public bool IsGhost { get; private set; }

    public Inventory Inventory;
    [SerializeField] private float characterSpeed;
    [SerializeField] private Animator animator;
    [SerializeField] private Transform model;
    [SerializeField] private Renderer characterRenderer;
    [SerializeField] private Renderer tailRenderer;
    [SerializeField] private Texture[] characterTextures;

    Rigidbody body;
    float initialSpeed;
    bool MoveActive;
    private Boat boat;

    public static event System.Action<int> onCurrencyUpdated;
    private void OnEnable()
    {
        InputManager.OnMoved += Handle_OnMoved;
        InputManager.OnKeyUp += Handle_OnKeyUp;
        InputManager.OnKeyDown += Handle_OnKeyDown;
        Shop.onSold += Shop_onSold;
    }
    private void OnDisable()
    {
        InputManager.OnMoved -= Handle_OnMoved;
        InputManager.OnKeyUp -= Handle_OnKeyUp;
        InputManager.OnKeyDown -= Handle_OnKeyDown;
        Shop.onSold -= Shop_onSold;
    }
    private void Awake()
    {
        body = GetComponent<Rigidbody>();
        initialSpeed = characterSpeed;
        currentPlace = Place.Port;
        MoveActive = true;
        IsAlive = true;
        IsGhost = false;
        onCurrencyUpdated?.Invoke(DirtCoin);
        SetTexture(false);
    }
    private void Handle_OnMoved(Vector3 dir)
    {
        if (!MoveActive || !IsAlive) return;

        //if ((int)CameraManager.Instance.CurrentCamera == 0)
        //    dir *= -1;
        Debug.DrawRay(transform.position, transform.forward, Color.green);
        Debug.DrawRay(transform.position, dir, Color.red);
        body.angularVelocity = Vector3.zero;
        if (Vector3.Dot(transform.forward, dir.normalized) <= -.999f)
        {
            dir += Vector3.Cross(Vector3.up, dir) * .5f;
        }
        var newDirection = Vector3.MoveTowards(transform.forward, dir.normalized, Time.deltaTime * characterSpeed * 2.5f);
        body.MoveRotation(Quaternion.LookRotation(newDirection));
        body.velocity = (newDirection.normalized * dir.magnitude) * characterSpeed;

        if (currentPlace != Place.Boat && currentPlace != Place.Ghost)
            animationState = AnimationState.Walk;
    }
    private void Handle_OnKeyUp()
    {
        body.angularVelocity = Vector3.zero;
        if (!MoveActive || !IsAlive) return;

        if (currentPlace != Place.Boat && currentPlace != Place.Ghost)
        {
            animationState = AnimationState.Idle;
            if (Vector3.Distance(transform.position, GameLogic.Instance.Rabbit.transform.position) < 2.25f)
            {
                CameraManager.Instance.SetCamera(VirtualCameras.ShopCamera);
                GameLogic.Instance.Rabbit.SetTalk();
                UIManager.Instance.SetShopStatus(true);
            }

        }
    }
    private void Handle_OnKeyDown()
    {
        if (Vector3.Distance(transform.position, GameLogic.Instance.Rabbit.transform.position) < 3f)
        {
            GameLogic.Instance.Rabbit.SetSit();
            UIManager.Instance.SetShopStatus(false);
            CameraManager.Instance.SetCamera(VirtualCameras.PortCamera);
        }

    }
    internal void SellAll()
    {
        int money = Inventory.SellAll();
        DirtCoin += money;
    }
    private void Shop_onSold()
    {
        SellAll();
    }
    internal void Die(float delay = 0f, System.Action callback = null)
    {
        IsAlive = false;
        animationState = AnimationState.Die;

        DOVirtual.DelayedCall(delay, () => callback?.Invoke());
    }
    internal void Ghost()
    {
        if (IsGhost) return;

        IsGhost = true;
        boat.SetFollowTransform(null);
        MoveActive = false;
        animationState = AnimationState.Die;

        DOVirtual.DelayedCall(4f, () =>
         {
             SetTexture(true);
             characterRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
             tailRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
             animationState = AnimationState.Ghost;
             currentPlace = Place.Ghost;
         });
    }
    internal void SetModelOffset(float heightOffset)
    {
        var modelPos = model.position;
        modelPos.y = heightOffset;
        model.position = modelPos;
    }
    void SetTexture(bool isDead)
    {
        if (isDead)
        {
            characterRenderer.sharedMaterial.SetTexture("_TextureSample3", characterTextures[1]);
        }
        else
        {
            characterRenderer.sharedMaterial.SetTexture("_TextureSample3", characterTextures[0]);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (!IsAlive) return;

        if (other.gameObject.layer == 7 && currentPlace == Place.Port && MoveActive)//boat
        {
            Boat b = other.GetComponent<Hitbox>().Owner.GetComponent<Boat>();
            if (boat == null)
                boat = b;
            if (!b.isEnable) return;

            MoveActive = false;
            animationState = AnimationState.Walk;
            transform.DOMove(b.SeatTransform.position, 1f)//.SetEase(Ease.Linear);
                .OnComplete(() =>
                {
                    animationState = AnimationState.Sit;
                    transform.DORotateQuaternion(Quaternion.LookRotation(b.SeatTransform.forward), .25f)
                    .OnComplete(() =>
                    {
                        b.SetFollowTransform(this.transform);
                        currentPlace = Place.Boat;
                        MoveActive = true;
                    });
                });
        }

        if (other.gameObject.layer == 8 && currentPlace == Place.Boat && MoveActive)//port
        {
            PortLocation pL = other.GetComponent<Hitbox>().GetComponent<PortLocation>();
            MoveActive = false;
            animationState = AnimationState.Walk;

            transform.DOMove(pL.PortTargetTransform.position, .25f)
                .OnComplete(() =>
                {
                    currentPlace = Place.Port;
                    animationState = AnimationState.Idle;
                    MoveActive = true;
                });
            boat.Locate2Port(other);
        }
        if (other.gameObject.layer == 6 && currentPlace == Place.Boat && MoveActive)//dirt
        {
            if (Inventory.isInventorySpaceAvailable)
            {
                Dirt d = other.GetComponent<Dirt>();
                if (!d.isCollected)
                {
                    d.SetTarget(transform);
                    //DOVirtual.DelayedCall(1f, () => inventory.Add(d));
                    Inventory.Add(d);
                }
            }
        }
    }
}
