using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DistanceBarController : MonoBehaviour
{
    public Slider barSlider;
    internal void SetValue(float distValue)
    {
        barSlider.value = distValue;
    }
}
