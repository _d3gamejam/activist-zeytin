using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public bool isInventorySpaceAvailable { get => inventoryList.Count < inventorySpace; }
    public bool isInventoryHasDirt { get => inventoryList.Count > 0; }
    [SerializeField] private int inventorySpace;

    private List<Dirt> inventoryList = new List<Dirt>();

    public static event System.Action<int> onInventoryChanged;
    internal void Add(Dirt dirt)
    {
        inventoryList.Add(dirt);
        onInventoryChanged?.Invoke(inventoryList.Count);
    }
    internal void Remove()
    {
        if (inventoryList.Count > 0)
        {
            Dirt d = inventoryList[0];
            inventoryList.RemoveAt(0);
            DirtGenerator.Instance.ReturnDirt(d);
            onInventoryChanged?.Invoke(inventoryList.Count);
        }
    }
    internal int SellAll()
    {
        var count = inventoryList.Count;
        if (count > 0)
        {
            for (int i = 0; i < count; i++)
            {
                Remove();
            }
        }
        return count;
    }
}
