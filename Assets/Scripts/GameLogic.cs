using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLogic : MonoBehaviour
{
    public static GameLogic Instance { get; private set; }
    public SceneTransition SceneTransition;
    public Rabbit Rabbit;
    public Character Character;
    public Boat Boat;
    public Transform PortTransform;

    [Space(20f)]
    public float minDistanceBoatToPort;
    public float maxDistanceBoatToPort;
    public float dirtDist;
    public float dirtDistForUse;

    int usedDirtCount = 0;
    private void Awake()
    {
        Instance = this;
    }

    private void Update()
    {
        if (!Character.IsAlive) return;

        var boatPos = Boat.transform.position;
        boatPos.y = 0f;
        var portPos = PortTransform.position;
        portPos.y = 0f;

        if (Vector3.Distance(boatPos, portPos) >= minDistanceBoatToPort)
        {
            var distValue = 1 - Mathf.InverseLerp(minDistanceBoatToPort, maxDistanceBoatToPort + (usedDirtCount * dirtDist), Vector3.Distance(boatPos, portPos));
            UIManager.Instance.SetDistanceValue(distValue);
            var offset = Boat.SetHeight(distValue);
            Character.SetModelOffset(offset);

            if (Character.Inventory.isInventoryHasDirt && distValue < dirtDistForUse)
            {
                Character.Inventory.Remove();
                usedDirtCount++;
            }

            if (distValue <= 0f && usedDirtCount < 10)
            {
                Character.Die(5f, () =>
                 {
                     SceneTransition.Transition();
                     //SceneTransition.SetVignetteValue(1f);
                     //UnityEngine.SceneManagement.SceneManager.LoadScene(1, UnityEngine.SceneManagement.LoadSceneMode.Single);
                 });
            }
            else if (distValue <= 0f)
            {
                Character.Ghost();
            }
        }
    }
    internal void SetUsedDirtCountZero()
    {
        usedDirtCount = 0;
    }
}
