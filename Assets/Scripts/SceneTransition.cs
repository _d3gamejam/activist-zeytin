using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using DG.Tweening;

public class SceneTransition : MonoBehaviour
{
    [SerializeField] PostProcessProfile Profile;
    [SerializeField] RoadCameraAnimationEvents cameraEvents;
    private void OnEnable()
    {
        if (cameraEvents)
            cameraEvents.onCameraMoveComplete += CameraEvents_onCameraMoveComplete;
        var value = Profile.GetSetting<Vignette>().intensity.value;
        if (value == 1f)
            Transition();
    }
    private void OnDisable()
    {
        if (cameraEvents)
            cameraEvents.onCameraMoveComplete -= CameraEvents_onCameraMoveComplete;
    }
    public void Transition()
    {
        var vignette = Profile.GetSetting<Vignette>();
        var value = vignette.intensity.value;
        DOTween.To(() => value, v => value = v, value == 1f ? 0f : 1f, 1f).SetEase(Ease.Linear)
            .OnUpdate(() =>
            {
                vignette.intensity.value = value;
            })
            .OnComplete(() =>
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene(1, UnityEngine.SceneManagement.LoadSceneMode.Single);
            });

    }
    private void CameraEvents_onCameraMoveComplete()
    {
        Transition();
    }
    internal void SetVignetteValue(float value)
    {
        var vignette = Profile.GetSetting<Vignette>();
        vignette.intensity.value = value;
    }
}
