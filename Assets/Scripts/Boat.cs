using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Boat : MonoBehaviour
{
    public bool isEnable { get; private set; }
    public Transform SeatTransform;
    private Transform followTransform;

    public float minHeightValue;

    private Vector3 followDirection;
    private Vector3 _initialPosition;
    private void Awake()
    {
        _initialPosition = transform.position;
        isEnable = true;
    }
    void Update()
    {
        if (followTransform)
        {
            var newPos = followTransform.position - followDirection;
            newPos.y = transform.position.y;
            transform.position = newPos;
            transform.rotation = followTransform.rotation;
        }
    }
    public void SetFollowTransform(Transform followObject = null)
    {
        if (followObject)
        {
            followDirection = SeatTransform.position - transform.position;
            followDirection.y = 0f;
        }
        followTransform = followObject;
    }

    internal void Locate2Port(Collider portCollider)
    {
        SetFollowTransform(null);
        isEnable = false;
        transform.DOMove(portCollider.transform.position, .5f).SetEase(Ease.Linear);
        var dir = portCollider.transform.forward;
        dir.y = 0f;
        dir.Normalize();
        transform.DORotateQuaternion(Quaternion.LookRotation(dir), .5f);
        DOVirtual.DelayedCall(5f, () => isEnable = true);
    }
    internal float SetHeight(float value)
    {
        var pos = transform.position;
        pos.y = Mathf.Lerp(minHeightValue, _initialPosition.y, value);
        transform.position = pos;

        return pos.y - _initialPosition.y;
    }
}
