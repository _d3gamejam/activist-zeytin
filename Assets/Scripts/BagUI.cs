using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
public class BagUI : MonoBehaviour
{
    public List<Image> lootImages;
    public int itemCount { get => lootImages.Count(i => i.gameObject.activeSelf); }
    private void OnEnable()
    {
        Inventory.onInventoryChanged += Inventory_onInventoryChanged;
    }
    private void OnDisable()
    {
        Inventory.onInventoryChanged -= Inventory_onInventoryChanged;
    }
    private void Inventory_onInventoryChanged(int count)
    {
        for (int i = 0; i < lootImages.Count; i++)
        {
            lootImages[i].gameObject.SetActive(count > i);
        }
    }
}
