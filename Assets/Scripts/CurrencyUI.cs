using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CurrencyUI : MonoBehaviour
{
    public TextMeshProUGUI CurrencyText;

    private void OnEnable()
    {
        Character.onCurrencyUpdated += Character_onCurrencyUpdated;    
    }
    private void OnDisable()
    {
        Character.onCurrencyUpdated -= Character_onCurrencyUpdated;
    }
    private void Character_onCurrencyUpdated(int value)
    {
        CurrencyText.text = value.ToString();
    }
}
