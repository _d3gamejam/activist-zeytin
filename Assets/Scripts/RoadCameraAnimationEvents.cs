using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadCameraAnimationEvents : MonoBehaviour
{
    internal event System.Action onCameraMoveComplete;
    public void CameraMoveComplete()
    {
        onCameraMoveComplete?.Invoke();
    }
}
