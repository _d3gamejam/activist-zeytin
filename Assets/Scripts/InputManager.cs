using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static event System.Action<Vector3> OnMoved;
    public static event System.Action OnKeyUp;
    public static event System.Action OnKeyDown;

    int inputCount = 0;
    private void Update()
    {
        var dir = Vector3.zero;
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D))
        {
            inputCount++;
            if (inputCount == 1)
                OnKeyDown?.Invoke();
        }

        if (Input.GetKey(KeyCode.W))
        {
            var tempDir = CameraManager.Instance.MainCamera.transform.forward;
            tempDir.y = 0f;
            tempDir.Normalize();
            dir += tempDir;
        }
        if (Input.GetKey(KeyCode.A))
        {
            var tempDir = CameraManager.Instance.MainCamera.transform.right;
            tempDir.y = 0f;
            tempDir.Normalize();
            dir += -tempDir;
        }
        if (Input.GetKey(KeyCode.S))
        {
            var tempDir = CameraManager.Instance.MainCamera.transform.forward;
            tempDir.y = 0f;
            tempDir.Normalize();
            dir += -tempDir;
        }
        if (Input.GetKey(KeyCode.D))
        {
            var tempDir = CameraManager.Instance.MainCamera.transform.right;
            tempDir.y = 0f;
            tempDir.Normalize();
            dir += tempDir;
        }
        dir.Normalize();
        if (dir.magnitude > 0)
            OnMoved?.Invoke(dir);

        if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.D))
        {
            inputCount--;
            if (inputCount == 0)
                OnKeyUp?.Invoke();
        }
    }
}
