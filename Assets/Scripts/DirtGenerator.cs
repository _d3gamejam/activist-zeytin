using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirtGenerator : MonoBehaviour
{
    public static DirtGenerator Instance { get; private set; }

    [SerializeField] private bool isGeneratorActive;
    [SerializeField] private Vector2 generationArea;
    [SerializeField] private float generationWidth;
    [SerializeField] private int activeDirtCount;
    [SerializeField] private float generationDuration = .5f;
    [SerializeField] private List<Dirt> dirtPool;

    private List<Dirt> _activeDirts = new List<Dirt>();
    private float tempTimer = 0f;

    private void Awake()
    {
        Instance = this;
    }
    private void Update()
    {
        if (isGeneratorActive)
        {
            tempTimer += Time.deltaTime;
            if (tempTimer >= generationDuration)
            {
                if (_activeDirts.Count < activeDirtCount)
                {
                    Dirt d = GetDirt();
                    if (d != null)
                    {
                        var tempPos = transform.position + new Vector3(
                            Random.Range(-generationWidth / 2f, generationWidth / 2f),
                            0f,
                            Random.Range(generationArea.x, generationArea.y)
                            );
                        d.transform.position = tempPos;
                        d.transform.localScale = Vector3.one;
                        d.transform.rotation = Quaternion.Euler(new Vector3(0f, Random.Range(0f, 360f), 0f));
                        d.isCollected = false;
                        d.gameObject.SetActive(true);
                    }
                }
            }
        }
    }
    internal Dirt GetDirt()
    {
        if (dirtPool.Count > 0)
        {
            Dirt d = dirtPool[0];
            _activeDirts.Add(d);
            dirtPool.RemoveAt(0);
            return d;
        }
        return null;
    }
    internal void ReturnDirt(Dirt dirt)
    {
        dirt.gameObject.SetActive(false);
        _activeDirts.Remove(dirt);
        dirtPool.Add(dirt);
    }
    internal void ReturnAll()
    {
        int count = _activeDirts.Count;
        for (int i = 0; i < count; i++)
        {
            ReturnDirt(_activeDirts[0]);
        }
    }
    private void OnDrawGizmos()
    {
        var posMin = transform.position;
        posMin.z += generationArea.x;
        var posMax = transform.position;
        posMax.z += generationArea.y;

        Gizmos.color = Color.magenta;
        var dirMin2Max = posMax - posMin;
        var centerPos = posMin + dirMin2Max / 2;
        Gizmos.DrawWireCube(centerPos, new Vector3(generationWidth, .1f, dirMin2Max.magnitude));
    }
}
