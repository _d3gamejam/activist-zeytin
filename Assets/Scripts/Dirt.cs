using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Dirt : MonoBehaviour
{
    public bool isCollected = false;
    private Transform tempTarget;
    void Update()
    {
        if (tempTarget)
        {
            var dir = (tempTarget.position + Vector3.up * .75f) - transform.position;
            transform.position += dir * Time.deltaTime * 3f;
        }
    }
    internal void SetTarget(Transform t = null)
    {
        tempTarget = t;
        if (tempTarget)
        {
            isCollected = true;
            transform.DOScale(Vector3.zero, 1f).SetEase(Ease.Linear)
                .OnComplete(() =>
                {
                    tempTarget = null;
                    gameObject.SetActive(false);
                });
        }

    }
}
