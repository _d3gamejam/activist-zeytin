using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    public Button sellButton;
    public BagUI bagUI;

    public static event System.Action onSold;
    private void Awake()
    {
        sellButton.onClick.AddListener(Handle_SellButton);
    }
    private void OnEnable()
    {
        if (bagUI.itemCount > 0)
            sellButton.interactable = true;
        else
            sellButton.interactable = false;
    }
    private void Handle_SellButton()
    {
        sellButton.interactable = false;
        onSold?.Invoke();
    }
}
