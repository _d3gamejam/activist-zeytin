using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public enum VirtualCameras
{
    FollowCamera,
    PortCamera,
    ShopCamera,
}
public class CameraManager : MonoBehaviour
{
    public static CameraManager Instance { get; private set; }
    
    public Camera MainCamera;
    public VirtualCameras CurrentCamera { get; private set; }

    [SerializeField] private List<CinemachineVirtualCamera> virtualCameras;
    public CinemachineVirtualCamera currentCameraObject { get; private set; }
    private void Awake()
    {
        Instance = this;
        DeactivateAllCameras();
        SetCamera(VirtualCameras.PortCamera);
    }
    void DeactivateAllCameras()
    {
        foreach (CinemachineVirtualCamera vc in virtualCameras)
        {
            vc.Priority = 1;
        }
    }
    internal void SetCamera(VirtualCameras camera)
    {
        if (CurrentCamera != camera)
        {
            CurrentCamera = camera;
            if (currentCameraObject != null)
                currentCameraObject.Priority = 1;
            currentCameraObject = virtualCameras[(int)CurrentCamera];
            currentCameraObject.Priority = 10;
        }
    }
}
