using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance { get; private set; }
    [SerializeField] GameObject shop;
    [SerializeField] DistanceBarController distanceBarController;
    private void Awake()
    {
        Instance = this;
    }
    public void SetShopStatus(bool status) 
    {
        shop.SetActive(status);
    }
    public void SetDistanceBarStatus(bool status)
    {
        distanceBarController.gameObject.SetActive(status);
    }

    internal void SetDistanceValue(float distValue)
    {
        distanceBarController.SetValue(distValue);
    }
}
